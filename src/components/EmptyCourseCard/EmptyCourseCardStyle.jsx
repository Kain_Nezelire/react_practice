import styled from "styled-components";

export const EmptyListContainer = styled.div`
text-align: center;
padding: 50px 20px;
`;

export const Title = styled.h2`
margin-bottom: 20px;
`;

export const Subtitle = styled.p`
margin-bottom: 20px;
color: #6c757d;
`;