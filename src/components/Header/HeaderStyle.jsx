import styled from "styled-components";

export const HeaderContainer = styled.header`
display: flex;
justify-content: space-between;
align-items: center;
padding: 10px 20px;
background-color: #ffffff;
border-bottom: 1px solid #e9ecef;
`;

export const LogoContainer = styled.div`
display: flex;
align-items: center;
`;

export const LogoText = styled.span`
margin-left: 10px;
font-size: 20px;
font-weight: bold;
color: #333;
`;

export const UserContainer = styled.div`
display: flex;
align-items: center;
`;

export const UserName = styled.span`
margin-right: 10px;
font-size: 16px;
color: #333;
`;