// src/components/Button.js
import React from 'react';
import {StyledButton} from "./ButtonStyle"



const Button = ({ buttonText, onClick, ...props }) => {
  return (
    <StyledButton onClick={onClick} {...props}>
      {buttonText}
    </StyledButton>
  );
};

export default Button;
