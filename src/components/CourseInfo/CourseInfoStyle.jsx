import styled from "styled-components";

export const InfoContainer = styled.div`
  padding: 20px;
`;

export const Title = styled.h2`
  margin-top: 0;
`;