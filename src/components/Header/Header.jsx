import React from 'react';
import Logo from '../Logo/Logo';
import Button from '../Button/Button';
import { HeaderContainer,LogoContainer,UserContainer,UserName } from './HeaderStyle';


const Header = () => {
return (
  <HeaderContainer>
    <LogoContainer>
      <Logo />
    </LogoContainer>
    <UserContainer>
      <UserName>Harry Potter</UserName>
      <Button buttonText="LOGIN" onClick={() => {}} />
    </UserContainer>
  </HeaderContainer>
);
};

export default Header;
