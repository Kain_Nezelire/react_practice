
// src/components/Courses.js
import React from 'react';
import CourseCard from '../CourseCard/CourseCard';
import EmptyCourseList from '../EmptyCourseCard/EmptyCourseCard';
import Button from '../Button/Button';
import { CoursesContainer,ButtonContainer } from './CoursesStyle';

const Courses = ({ courses, authors }) => {
  return (
    <CoursesContainer>
      <ButtonContainer>
        <Button buttonText="Add New Course" onClick={() => {}} />
      </ButtonContainer>
      {courses.length === 0 ? (
        <EmptyCourseList />
      ) : (
        courses.map((course) => <CourseCard key={course.id} course={course} authors={authors} />)
      )}
    </CoursesContainer>
  );
};

export default Courses;
