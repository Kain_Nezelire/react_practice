import styled from "styled-components";

export const CardContainer = styled.div`
  display: flex;
  justify-content: space-between;
  border: 1px solid #e9ecef;
  border-left: 5px solid black; 
  border-radius: 4px;
  padding: 20px;
  margin: 40px 100px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
`;

export const LeftContent = styled.div`
  flex: 1;
  margin-right:200px;
`;

export const RightContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  max-width:20%;
    div{
        display:flex;
        gap:10px;
    }
`;

export const Title = styled.h2`
  margin-top: 0;
  margin-bottom: 10px;
  font-size: 24px;
  font-weight: bold;
`;

export const Description = styled.p`
  color: black;
`;

export const InfoText = styled.p`
  margin: 5px 0;
  font-weight: bold;
`;
