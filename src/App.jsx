import React, { useState } from 'react';
import styled from 'styled-components';
import Header from '../src/components/Header/Header';
import Courses from '../src/components/Courses/Courses';
import CourseInfo from '../src/components/CourseInfo/CourseInfo';
import { mockedCoursesList, mockedAuthorsList } from '../src/data/mockData';

const AppContainer = styled.div`
  font-family: 'Open Sans', sans-serif;
`;



const App = () => {
  const [courses, setCourses] = useState(mockedCoursesList);
  const [selectedCourse, setSelectedCourse] = useState(null);

  return (
    <AppContainer>
      <Header />
      {selectedCourse ? (
        <CourseInfo course={selectedCourse} authors={mockedAuthorsList} onBack={() => setSelectedCourse(null)} />
      ) : (
        <Courses courses={courses} authors={mockedAuthorsList} />
      )}
    </AppContainer>
  );
};

export default App;
