import styled from "styled-components";

export const CoursesContainer = styled.div`
  padding: 20px;
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 20px 100px 0px 100px; 
  `
