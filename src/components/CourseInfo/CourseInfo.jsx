// src/components/CourseInfo.js
import React from 'react';
import Button from '../Button/Button';
import { InfoContainer , Title } from './CourseInfoStyle';

const CourseInfo = ({ course, authors, onBack }) => {
  const { id, title, description, creationDate, duration, authors: courseAuthors } = course;

  const formatDuration = (minutes) => {
    const hours = Math.floor(minutes / 60);
    const mins = minutes % 60;
    return `${hours < 10 ? '0' : ''}${hours}:${mins < 10 ? '0' : ''}${mins} ${hours === 1 ? 'hour' : 'hours'}`;
  };

  const getAuthorsNames = (authorIds) => {
    return authorIds
      .map((id) => {
        const author = authors.find((author) => author.id === id);
        return author ? author.name : '';
      })
      .join(', ');
  };

  return (
    <InfoContainer>
      <Title>{title}</Title>
      <p>ID: {id}</p>
      <p>{description}</p>
      <p>Duration: {formatDuration(duration)}</p>
      <p>Created: {creationDate}</p>
      <p>Authors: {getAuthorsNames(courseAuthors)}</p>
      <Button buttonText="Back to courses" onClick={onBack} />
    </InfoContainer>
  );
};

export default CourseInfo;
