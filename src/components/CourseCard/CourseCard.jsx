
import React from 'react';

import Button from '../Button/Button';
import { CardContainer,LeftContent,RightContent,Title,Description,InfoText } from './CourseCardStyle';


const CourseCard = ({ course, authors }) => {
  const { title, description, creationDate, duration, authors: courseAuthors } = course;

  const formatDuration = (minutes) => {
    const hours = Math.floor(minutes / 60);
    const mins = minutes % 60;
    return `${hours < 10 ? '0' : ''}${hours}:${mins < 10 ? '0' : ''}${mins} ${hours === 1 ? 'hour' : 'hours'}`;
  };

  const getAuthorsNames = (authorIds) => {
    return authorIds
      .map((id) => {
        const author = authors.find((author) => author.id === id);
        return author ? author.name : '';
      })
      .join(', ');
  };

  return (
    <CardContainer>
      <LeftContent>
        <Title>{title}</Title>
        <Description>{description}</Description>
      </LeftContent>
      <RightContent>
        <InfoText>Authors: {getAuthorsNames(courseAuthors)}</InfoText>
        <InfoText>Duration: {formatDuration(duration)}</InfoText>
        <InfoText>Created: {creationDate}</InfoText>
        <div>
          <Button buttonText="Show course" onClick={() => {}} />
          <Button buttonText="Delete" onClick={() => {}} />
          <Button buttonText="Edit" onClick={() => {}} />
        </div>
      </RightContent>
    </CardContainer>
  );
};

export default CourseCard;
