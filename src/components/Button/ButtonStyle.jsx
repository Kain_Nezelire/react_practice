import styled from "styled-components";


export const StyledButton = styled.button`
  padding: 10px 20px;
  background-color: #007298;
  color: white;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  font-size: 16px;
  &:hover {
    background-color: #007298;
  }
  &:focus {
    outline: none;
  }
`;